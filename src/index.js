import ReactDOM from "react-dom/client";
import {
    BrowserRouter,
    Routes,
    Route,
} from "react-router-dom";
// import your route components too
import App from "./App";
import Home from "./pages/Home";
import ProductDetail from "./pages/ProductDetail";
const root = ReactDOM.createRoot(
    document.getElementById("root")
);
root.render(
    <BrowserRouter>
        <App />
    </BrowserRouter>
);