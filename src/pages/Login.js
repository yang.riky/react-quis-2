
import {useState} from "react";
import { useNavigate } from "react-router-dom";

const Login = () => {
    const [user, setUser] = useState('')
    let navigate = useNavigate();
    console.log('user', user)
    return (
        <div className="container mx-auto">
            <div>
                <input style={{ borderWidth: 1, borderColor: 'red'}} type="text" onChange={(event) => setUser(event.target.value)}/>
                <button onClick={() => {
                    localStorage.setItem('user', user);
                    navigate('/')
                }}>Login</button>
            </div>
        </div>
    )
}

export default Login;