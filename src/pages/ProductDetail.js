import {Link, useParams} from 'react-router-dom';
import { useEffect, useState } from "react";
import axios from 'axios';
const API = 'https://api.lemonilo.com/v1/'
const PRODUCT = 'product/'
const ProductDetail = (props) => {

    const [product, setProduct] = useState();
    const params = useParams();
    const { product_slug } = params

    console.log('params', params);
    const getProductDetail = async () => {
        try {
            console.log('product_slug', product_slug)
            const { data } =  await axios.get(API + PRODUCT + product_slug)
            setProduct(data.data)
            console.log('data', data)
        } catch (e) {
            // silent e
        }
    }
    useEffect(() => {
        getProductDetail();
    }, [])
    console.log('product', product);
    return (
        <div className="container mx-auto">
            <div className="card w-full flex flex-row" >
                <div className="flex-1">
                    <img src={product?.productImages[0]['photoUrl']} alt="Avatar" style={{ width: 470, height: 'auto' }} />
                </div>
                <div className="flex-1">
                    <div>
                        <p>{product?.merchant?.name}</p>
                        <h4><b>
                            <span>name: {product?.name}</span> <br/>
                        </b></h4>
                        <p>{product?.actualWeight * 1000} gr</p>
                        <div className="flex flex-row">
                            <div className="flex-shrink">
                                 <span style={{ textDecoration: 'line-through', color:  product?.gimmickPrice ? 'blue' : 'red'}}>
                                      {product?.gimmickPrice}
                                </span>
                            </div>
                            <div className="flex-1">
                                {product?.finalSellPrice}
                            </div>
                        </div>
                        {/*<p>*/}

                        {/*</p>*/}
                        <button >Add To Cart</button>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default ProductDetail;