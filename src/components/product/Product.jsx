import React from "react";
import './Product.css';
import { Link } from "react-router-dom";

const Product = ({ products, addToCart }) => {
    return (
        <div className="product flex flex-row w-full flex-wrap space-x-6 justify-between">
            {products.map((product, index) => (
                <div className="card w-3/12" key={index}>
                    <Link to={`product/${product.slug}`}>
                        <img src={product?.photoUrl} alt="Avatar" style={{ width: '100%' }} />
                        <div>
                            <h4><b>
                                <span>name: {product.name}</span> <br/>
                            </b></h4>
                            <p>
                                <span style={{ textDecoration: 'line-through', color:  product.gimmickPrice ? 'blue' : 'red'}}>
                                      price: {product.gimmickPrice}
                                </span>
                            </p>
                            <button onClick={() => alert(index)}>Add To Cart</button>
                        </div>
                    </Link>
                </div>
            ))}
        </div>
    )
}

export default React.memo(Product);